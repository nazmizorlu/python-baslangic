# Decorators

def my_wrapper(fn):
    def wrapper(fn):
        print("Once")
        fn()
        print("sonra")
    return wrapper(fn=fn)


@my_wrapper
def my_function():
    print("Merhaba")


my_function()