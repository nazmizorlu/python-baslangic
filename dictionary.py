# dict type
d1 = {}
d2 = dict()

d3 = {
    "ad": "Python",
    "yas": 25,
    "popularite": 2,
    "dev": [
        "Guido",
        "Moss"
    ]
}

print(d3["ad"])

for eleman in d3.items():
    print(eleman)

for key, deger in d3.items():
    print(key, ": ", deger)

for eleman in d3.items():
    print(eleman[0], ": ", eleman[1])

for eleman in d3:
    print(eleman)

for eleman in d3:
    print(eleman, ": ", d3[eleman])

yas = d3.pop("yas")

son_eleman = d3.popitem()

d3.popitem()

d3["popularite"] = 2

d3.update(
    {
        "popularite": 1,
        "yas": 30
    }
)
