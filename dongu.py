
def puan_hesapla(puan):
    if puan < 20:  # False
        return 'FF'
    elif puan < 50:  # False
        return 'DD'
    elif puan < 70:
        return 'CC'
    elif puan < 80:
        return 'BB'
    elif puan <= 100:
        return 'AA'
    else:
        print("Yok boyle bir puan")


def gecti_kaldi(ogrenci_listesi):
    for ogrenci in ogrenci_listesi:
        ogrenci_harfi = puan_hesapla(ogrenci['sinav_notu'])
        if ogrenci_harfi == 'FF':
            print("Ogrenci Kaldi: %s" % ogrenci['adi_soyadi'])
        elif ogrenci_harfi == 'DD':
            print("Ogrenci Sartli Gecti: %s" % ogrenci['adi_soyadi'])
        elif ogrenci_harfi in ['CC', 'BB']:
            print("Ogrenci Gecti: %s" % ogrenci['adi_soyadi'])
        elif ogrenci_harfi == 'AA':
            print("Ogrenciyi alnindan op: %s" % ogrenci['adi_soyadi'])
        else:
            print("Ogrenci notunu kontrol et %s" % ogrenci['adi_soyadi'])
