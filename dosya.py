# Dosya Islemleri

f = open("dosya_1.txt", 'w')
f.write("Satir 1\n")
f.write("Satir 2\n")
f.close()


satirlar = ["Satir 3\n", "Satir 4\n"]
f = open("dosya_2.txt", 'w')
f.writelines(satirlar)


f2 = open("dosya_2.txt", "w")
f2.write("Test")
f2.close()
f.close()

with open("dosya_2.txt", 'a') as f:
    f.write("Son Satir")

with open("dosya_2.txt", "r") as f3:
    icerik = f3.read()
    print("Dosya icerigi: ")
    print(icerik)

with open("dosya_2.txt", "r") as f4:
    icerik = f4.readlines()
    i = 1
    for satir in icerik:
        print("{}. satir: {}".format(i, satir))
        i += 1

print("Is bitti")
