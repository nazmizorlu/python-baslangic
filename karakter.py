metin = "Python Version 3"

for i in metin:
    print(i)

print(metin[5])
print(metin[7:9])
print(metin[:-1])
print(metin[-3:-1])
print(metin[-3:])

kelimeler = metin.split('o')
print(kelimeler)

print("{} * {} = {}".format(4, 5, 4 * 5))
print("{1} * {0} = {2}".format(4, 5, 4 * 5))

print("{sayi2} * {sayi1} = {sayi3}".format(sayi3=(4 * 5), sayi1="dort", sayi2=5))

print("'%s' gecerli degil." % "ABC")
print("'%d' gecerli degil." % 3)
# print("'%d' gecerli degil." % 'ABC')
print("'%.2f' gecerli degil." % 3.14156789)
print("'%.2f' gecerli degil. Ornek: %s" % (3.14156789, "8.8345"))


print("deneme yapiyoruz".capitalize())

a = "deneme yApiyoruz"
print(a.lower())
print(a.upper())

a = "deneme XApiyoruz"
print(a.center(30))
print(a.ljust(30))
print(a.rjust(30))
print(a.endswith('x'))
print(a.endswith('z'))

metin_2 = "Tutar: 50 TL"
print(metin_2.startswith('Tutar: '))
print(metin_2.endswith('TL'))
print(metin_2.endswith('USD'))
print(a.find('TL'))


print(metin_2[metin_2.find(':'):])
print(metin_2[metin_2.find(':') + 2:])
print(metin_2.count('T'))
