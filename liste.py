# Listeler

# Liste tanimlama
liste_1 = list()
liste_2 = []

print(type(liste_1))
print(type(liste_2))

# Liste gosterimi
print(liste_1)

# Liste elemanlari uzerinde donme (iteration)
liste_3 = [1, 2, 3, 4, 5, "a", "b", [5, 6, 7]]
for eleman in liste_3:
    print(eleman)

# Index'i bilinen elemana erisim
print(liste_3[5])

# Listeye eleman ekleme
liste_1.append('qw')
print(liste_1)

sayi_listesi = []
for sayi in range(0, 200000):
    if sayi % 3 == 0:
        sayi_listesi.append(sayi)

# List Comprehensions
sayi_listesi = [sayi for sayi in range(0, 200000) if sayi % 3 == 0]


if sayi == 3:
    a = "buyuk"
else:
    a = "Kucuk"

a = sayi == 3 and "buyuk" or "Kucuk"


if a == 1:
    print("1")
    print("c)")

print("iki")

print("a")


Degisken = "1"
degisken_adi = " a"


a = "bir"
b = "uc"

a = "iki"

a = b

# List comprehension
c = [{"ad": "a", "c": 2}, {"ad": "ahmet", "c": 4}]
ad_listesi = [x['ad']*x['c'] for x in c]

a = ""
a = 9
a = [1, 2]


def fn1(max_sayi):
    liste = []
    for a in range(max_sayi):
        liste.append(a**2)
    return liste


def fn2(max_sayi):
    return [x**2 for x in range(max_sayi)]


def fn3(max_sayi):
    return [x for x in map(lambda a: a**2, range(max_sayi))]








