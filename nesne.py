# Nesneler
from alt_modul.alt_nesneler import yazdir

class Okul:
    ad = None

    def __init__(self, okul_adi):
        self.ad = okul_adi

    def __str__(self):
        return "Okul: {}".format(self.ad)

    def __repr__(self):
        return self.__str__()


class Sinif:
    ad = None
    okul = None
    ogrenciler = []

    def __init__(self, sinif_adi, _okul=None):
        self.ad = sinif_adi
        if _okul is not None and isinstance(_okul, Okul):
            self.okul = _okul
        else:
            raise Exception("Okulu duzgun ver.")

    def __iter__(self):
        self.ogrenci_idx = 0
        return self

    def __next__(self):
        current_index = self.ogrenci_idx
        if current_index < len(self.ogrenciler):
            self.ogrenci_idx += 1
            return self.ogrenciler[current_index]
        else:
            raise StopIteration()

    def __str__(self):
        return "{}: {}".format(
            self.okul.ad,
            self.ad
        )

    def __repr__(self):
        return "{} {}".format(
            self.__class__,
            self.__str__()
        )


class Ogrenci:
    ad = ""
    soyad = ""
    numara = 0
    sinif = None

    def __init__(self, ad, soyad, numara, sinif):
        self.ad = ad
        self.soyad = soyad
        self.numara = numara
        self.sinif = sinif

    @classmethod
    def class_fonksiyonu(cls):
        return cls.numara


def okul_yazdir(okul):
    print(okul.ad)
