# Sart ifadeleri

if 0:
    print("Sifir")
else:
    print("Degil")

sayi = 3

if sayi > 3:
    print("3'ten buyuk")
elif sayi < 3:
    print("3'ten kucuk")
else:
    print("3'e esit")


a = [1, 2, 3]
for index in range(5):
    if index >= len(a):
        print("Bir alt satirda 'IndexError' hatasi olusacak.")
    print(a[index])

try:
    for index in range(5):
        print(a[index])
except:
    print("Liste bitti.")


try:
    print("Deneme")
except:
    pass

for index in range(5):
    try:
        print(a[index][0])
    except IndexError:
        print("Liste elemanlari bitti")
    except TypeError:
        print("Veri: '{}' istedigimiz tipte degil".format(a[index]))
    except:
        print("Bilinmeyen hata.")
    finally:
        print("Finally blok")


